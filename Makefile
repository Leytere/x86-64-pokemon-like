make:
	yasm -g dwarf2 -f elf64 ./src/main.asm -l ./lst/main.lst
	yasm -g dwarf2 -f elf64 ./src/utils/io_operations.asm -l ./lst/utils/io_operations.lst
	yasm -g dwarf2 -f elf64 ./src/initialisation/game_start.asm -l ./lst/initialisation/game_start.lst
	yasm -g dwarf2 -f elf64 ./src/utils/utils.asm -l ./lst/utils/utils.lst
	yasm -g dwarf2 -f elf64 ./src/initialisation/char_creation.asm -l ./lst/initialisation/char_creation.lst
	yasm -g dwarf2 -f elf64 ./src/utils/monsters_id_to_path.asm -l ./lst/utils/monsters_id_to_path.lst
	yasm -g dwarf2 -f elf64 ./src/monsters/status.asm -l ./lst/monsters/status.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/monster.asm -l ./lst/constructors_destructors/monster.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/storage_box.asm -l ./lst/constructors_destructors/storage_box.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/storage.asm -l ./lst/constructors_destructors/storage.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/team.asm -l ./lst/constructors_destructors/team.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/misc.asm -l ./lst/constructors_destructors/misc.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/inventory.asm -l ./lst/constructors_destructors/inventory.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/item.asm -l ./lst/constructors_destructors/inventory.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/game.asm -l ./lst/constructors_destructors/game.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/town.asm -l ./lst/constructors_destructors/town.lst
	yasm -g dwarf2 -f elf64 ./src/constructors_destructors/npc.asm -l ./lst/constructors_destructors/npc.lst
	yasm -g dwarf2 -f elf64 ./src/towns/town_init.asm -l ./lst/towns/town_init.lst
	yasm -g dwarf2 -f elf64 ./src/towns/data_town_narfolk.asm -l ./lst/towns/data_town_narfolk.lst
	yasm -g dwarf2 -f elf64 ./src/towns/towns_array.asm -l ./lst/towns/towns_array.lst
	yasm -g dwarf2 -f elf64 ./tests/tests.asm -l ./lst/tests/tests.lst
	yasm -g dwarf2 -f elf64 ./tests/constructors_destructors/storage.asm -l ./lst/tests/constructors_destructors/storage.lst -o test_storage.o
	ld -g -o poke_like main.o io_operations.o game_start.o utils.o char_creation.o monsters_id_to_path.o status.o monster.o storage_box.o storage.o \
		team.o misc.o inventory.o item.o game.o town_init.o data_town_narfolk.o towns_array.o town.o npc.o
	gcc -g -no-pie -o ./tests/tests tests.o test_storage.o io_operations.o utils.o monster.o storage_box.o status.o storage.o
	rm main.o game_start.o char_creation.o monsters_id_to_path.o team.o misc.o storage.o inventory.o item.o tests.o test_storage.o io_operations.o \
		utils.o monster.o storage_box.o status.o game.o town_init.o data_town_narfolk.o towns_array.o town.o npc.o

clean:
	rm ./lst/main.lst ./lst/utils/io_operations.lst ./lst/initialisation/game_start.lst ./lst/utils/utils.lst ./lst/initialisation/char_creation.lst \
	  ./lst/utils/monsters_id_to_path.lst ./lst/monsters/status.lst ./lst/constructors_destructors/monster.lst ./lst/constructors_destructors/storage_box.lst \
	  ./lst/constructors_destructors/storage.lst ./lst/constructors_destructors/team.lst ./lst/constructors_destructors/misc.lst \
		./lst/constructors_destructors/inventory.lst ./lst/constructors_destructors/item.lst ./lst/tests/constructors_destructors/storage.lst ./lst/tests/tests.lst \
		./lst/constructors_destructors/game.lst ./lst/towns/town_init.lst ./lst/towns/data_town_narfolk.lst ./lst/towns/towns_array.lst \
		./lst/constructors_destructors/town.lst ./lst/constructors_destructors/npc.lst
