;------------
section .data

errNumArgMsg   db "You must provide the test name.", LF, NULL
errFalseArgMsg db "The provided test name is incorrect.", LF, NULL

%include "../src/utils/constants.asm"
%include "tests_mapping.asm"

extern PrintString

;------------
section .text

global main
main:
    nop 

    cmp    rdi, 1
    je     .ErrNumArg

    xor    rcx, rcx
    mov    r8, 1

.TestsLoop:
    mov    rax, qword [rsi+(r8*8)]
    mov    dl, byte [rax]

    mov    rdi, qword [testMapping+(rcx*8)]
    cmp    dl, byte [rdi]
    je     .TestString

    inc    rcx
    cmp    rcx, NUM_TESTS
    ja     .ErrFalseArg 
    jmp    .TestsLoop

.TestString:
    inc    rdi
    cmp    byte [rdi], 0
    jne    .ContinueTestString
    
    inc    rax
    mov    dl, byte [rax]
    cmp    dl, 0
    jne    .ContinueOuterLoop

    mov    rax, qword [addrMapping+(rcx*8)]
    jmp    rax

.End:
    mov    rax, SYS_EXIT
    mov    rdi, EXIT_SUCCESS
    syscall

.ContinueTestString:
    inc    rax
    mov    dl, byte [rax]
    cmp    dl, byte [rdi]
    je     .TestString

.ContinueOuterLoop:
    inc    rcx
    cmp    rcx, NUM_TESTS
    ja     .ErrFalseArg
    jmp    .TestsLoop

.ErrNumArg:
    mov    rdi, errNumArgMsg
    xor    rsi, rsi
    call   PrintString
    jmp    .End

.ErrFalseArg:
    mov    rdi, errFalseArgMsg
    xor    rsi, rsi
    call   PrintString
    jmp    .End
