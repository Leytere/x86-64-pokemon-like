;------------
section .data

%include "../../src/utils/constants.asm"

space               db " ", NULL
testMonsterStartMsg db "Testing Monster n°", NULL
testStBoxStartMsg   db "Testing StorageBox n°", NULL
testStorageStartMsg db "Testing Storage", LF, LF, NULL

;------------
section .bss

extern Malloc
extern Free
extern NewStructMonster
extern NewStructStorageBox
extern NewStructStorage
extern FreeStructStorage
extern PrintStringSlow
extern PrintStringInstant
extern PrintUnsignedNumber
extern PrintNewLine
extern DisplayStats

;------------
section .text

; TestCDStorage
; Test the instanciation of a Storage structure.
;
global TestCDStorage
TestCDStorage:
    mov    rdi, testStorageStartMsg
    call   PrintStringSlow
    call   PrepareStorageBoxArray
    mov    rdi, rax
    call   NewStructStorage
    mov    rbx, rax
    xor    r12, r12

.Loop:
    mov    rdi, qword [rbx+(r12*8)]
    mov    rsi, r12
    call   TestStorageBox
    inc    r12
    cmp    r12, 20
    jne    .Loop

    mov    rdi, rbx
    call   FreeStructStorage

    ret

; PrepareStorageBoxArray
; Prepare the StorageBox array for the Storage constructor.
;
PrepareStorageBoxArray:
    push   rbx
    push   r12

    mov    rdi, STORAGE_BOX_SIZE*20
    call   Malloc
    mov    rbx, rax
    xor    r12, r12

.Loop:
    call   PrepareMonsterArray              ; rbx = address of the array
    mov    rdi, rax                         ; r12 = loop index
    call   NewStructStorageBox
    mov    qword [rbx+(r12*8)], rax
    inc    r12
    cmp    r12, 20
    jne    .Loop

    mov    rax, rbx
    pop    r12
    pop    rbx
    ret

; PrepareMonsterArray
; Prepare the Monster array for the StorageBox constructor. 
;
; Returns:
;   rax - address - address of the monster array
;
PrepareMonsterArray:
    push   rbx
    push   r12
    push   r13

    mov    rdi, MONSTER_SIZE*20
    call   Malloc
    mov    r13, rax

    xor    rbx, rbx
    mov    r12, 1

.Loop:
    xor    rdi, rdi                  ; r12 = monster level index
    mov    rsi, r12                  ; rbx = loop index
    xor    rdx, rdx                  ; r13 = address of the array
    call   NewStructMonster

    mov    qword [r13+(rbx*8)], rax
    inc    r12
    inc    rbx
    cmp    rbx, 20
    jne    .Loop

    mov    rax, r13
    pop    r13
    pop    r12
    pop    rbx
    ret

; TestMonster
; Test a newly created Monster structure.
;
; Params:
;   rdi - address - address of the Monster structure
;   rsi - value   - number of the monster
;
; Returns:
;   rax - address - address of the Monster structure
;
TestMonster:
    push   rbx
    push   r12
    mov    rbx, rdi
    mov    r12, rsi

    mov    rdi, testMonsterStartMsg
    call   PrintStringSlow
    mov    rdi, r12
    push   0
    call   PrintUnsignedNumber
    call   PrintNewLine
    
; Id & lvl print
    movzx  edi, byte [rbx]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    movzx  edi, byte [rbx+OFF_LVL]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    
; Name & stats print
    mov    rdi, qword [rbx+OFF_MONSTER_NAME]
    call   PrintStringInstant
    mov    rdi, qword [rbx+OFF_STATS]
    call   DisplayStats

; Coeff & coeff impact print
    mov    r12, qword [rbx+OFF_STATS]
    mov    edi, dword [r12+OFF_COEF]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    movzx  edi, word [r12+OFF_COEF_IMPACT]
    push   0
    call   PrintUnsignedNumber
    call   PrintNewLine

; Hp, mp, xp print
    mov    rdi, qword [rbx+OFF_CUR_HP]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    mov    rdi, qword [rbx+OFF_MAX_HP]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    mov    rdi, qword [rbx+OFF_CUR_MP]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    mov    rdi, qword [rbx+OFF_MAX_MP]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    mov    rdi, qword [rbx+OFF_CUR_XP]
    push   0
    call   PrintUnsignedNumber
    mov    rdi, space
    call   PrintStringInstant
    mov    rdi, qword [rbx+OFF_XP_TO_LVL]
    push   0
    call   PrintUnsignedNumber
    call   PrintNewLine
    
    mov    rax, rbx
    pop    r12
    pop    rbx
    ret

; TestStorageBox
; Test a StorageBox instanciation.
;
; Params:
;   rdi - address - address of the StorageBox structure
;   rsi - value   - number of the StorageBox
;
; Returns:
;   rax - address- address of the StorageBox structure
;
TestStorageBox:
    push   rbx
    push   r12
    mov    rbx, rdi
    mov    r12, rsi

    mov    rdi, testStBoxStartMsg
    call   PrintStringSlow
    mov    rdi, r12
    push   0
    call   PrintUnsignedNumber
    call   PrintNewLine

    xor    r12, r12

.Loop:
    mov    rdi, qword [rbx+(r12*8)]                  ; rbx = address of StorageBox
    mov    rsi, r12
    call   TestMonster                               ; r12 = loop index
    inc    r12
    cmp    r12, 20
    jne    .Loop
    
    mov    rax, rbx
    pop    r12
    pop    rbx
    ret
