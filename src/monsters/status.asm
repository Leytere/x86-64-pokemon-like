; GS_GetStat
; Calculate one stat: round((Level*StatCoeff(+||-)Level/2*RandomCoeff))
;
; Params:
;   %1   - value  - offset of the stat structure
;
%macro GS_GetStat 1
    movss    xmm3, dword [monstersStatsCoeff+(rdi*4)+%1]
    mulss    xmm3, xmm1          ; xmm0 - random coefficient
    movss    xmm4, xmm1          ; xmm1 - level of the monster
    divss    xmm4, xmm2          ; xmm2 - value - 2
    mulss    xmm4, xmm0          ; xmm3 - Level*StatCoeff
                                 ; xmm4 - Level/2*RandomCoeff
    cmp      si, UINT_16_MIDDLE  ; rdi  - id of the monster 
    jb       %%Negative          ; si   - coefficient impact
                                 ; rdx  - Stats struct address
    addss    xmm3, xmm4
    jmp      %%End

%%Negative:
    subss    xmm3, xmm4

%%End:
    cvtss2si eax, xmm3
    mov      dword [rdx+%1], eax
%endmacro 

; DS_DisplayLine
; Display a line in the DisplayStats subroutine.
;
; Params:
;   %1 - message address
;   %2 - stat offset
;
%macro DS_DisplayLine 2
    mov      rdi, %1
    call     PrintStringSlow
    mov      edi, dword [rbx+%2]
    push     0
    call     PrintUnsignedNumber
    call     PrintNewLine
%endmacro

; DS_CmpGrowth
; Compare the coef with the growth value.
;
; Params:
;   %1 - growthBreaks offset
;   %2 - address of the positive message
;   %3 - address of the negative message
;
%macro DS_CmpGrowth 3
    movss    xmm1, dword [growthBreaks+%1]
    ucomiss  xmm0, xmm1
    jb       %%IsInRange
    jmp      %%End

%%IsInRange:
    cmp      si, UINT_16_MIDDLE
    jb       %%Negative
    mov      rdi, %2
    jmp      DS_End

%%Negative:
    mov      rdi, %3
    jmp      DS_End

%%End:
%endmacro

;------------
section .data

%include "../utils/constants.asm"
%include "monster_status_data.asm"

growthBreaks  dd 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0

vitMsg        db LF, "Vit: ", NULL
dexMsg        db "Dex: ", NULL
strMsg        db "Str: ", NULL
defMsg        db "Def: ", NULL
intMsg        db "Int: ", NULL
spdMsg        db "Spd: ", NULL
coeffMsg      db "Growth: ", NULL
growthNullMsg db "Absolute failure", NULL
growthBe9Msg  db "Less then worthless", NULL
growthBe8Msg  db "Worthless", NULL
growthBe7Msg  db "Awful", NULL
growthBe6Msg  db "Terrible", NULL
growthBe5Msg  db "Very weak", NULL
growthBe4Msg  db "Weak", NULL
growthBe3Msg  db "Almost a weakling", NULL
growthBe2Msg  db "Strongly below average", NULL
growthBe1Msg  db "Below average", NULL
growthAveMsg  db "Average", NULL
growthAb1Msg  db "Above average", NULL
growthAb2Msg  db "Strongly above average", NULL
growthAb3Msg  db "Strong", NULL
growthAb4Msg  db "Very strong", NULL
growthAb5Msg  db "Great", NULL
growthAb6Msg  db "Mighty", NULL
growthAb7Msg  db "Heroic", NULL
growthAb8Msg  db "DemiGod-like", NULL
growthAb9Msg  db "Godlike", NULL
growthPerfMsg db "Absolute perfection", NULL

;------------
section .bss

extern PrintStringSlow
extern PrintUnsignedNumber
extern PrintNewLine
extern Malloc

;------------
section .text

; GetStats
; Get the calculated stats of a monster given a correct structure.
; If the stat attribute is null a new Stats structure will be initialised.
;
; Params:
;   rdi - address - address of the monster structure
;
; Returns:
;   rax - address - address of the monster structure
;
global GetStats
GetStats: 
    push     rbx
    mov      rbx, rdi

    movzx    rsi, byte [rdi+OFF_LVL]
    push     rsi
    movzx    rsi, byte [rdi]
    push     rsi
    mov      rdx, qword [rdi+OFF_STATS]

    cmp      rdx, 0
    je       .RandLoop
    push     rdx
    jmp      .CalculateStats

.RandLoop
    rdrand   ax                    ; Get random number between 0 and 1000
    cmp      ax, UINT_16_MAXR_1000 ;   and store in xmm1.
    ja       .RandLoop
    mov      cx, UINT_16_R1000_DIV
    xor      rdx, rdx
    div      ax, cx
    movzx    ecx, ax
    cvtsi2ss xmm1, ecx

.SecondRandLoop:
    rdrand   ax                    ; Get random to determine if the random coeff
    cmp      ax, 0                 ;   will impact positively or negatively the stat.
    je       .SecondRandLoop
    cmp      ax, UINT_16_MAX
    je       .SecondRandLoop
    push     rax

    mov      edx, 0                ; Divide the random number by 1000
    cvtsi2ss xmm0, edx             ;   and add it to 0 to make the 
    mov      edx, 1000             ;   random coeff.
    cvtsi2ss xmm2, edx

    divss    xmm1, xmm2
    addss    xmm0, xmm1

    mov      rdi, STATS_SIZE
    call     Malloc
    mov      qword [rbx+OFF_STATS], rax

.CalculateStats:
    mov      esi, 2
    cvtsi2ss xmm2, esi
    pop      rsi
    pop      rdi                   ; Id of the monster
    pop      rax                   ; Level of the monster
    cvtsi2ss xmm1, rax
    mov      rdx, qword [rbx+OFF_STATS]

    GS_GetStat 0 
    GS_GetStat OFF_DEX
    GS_GetStat OFF_STR
    GS_GetStat OFF_DEF
    GS_GetStat OFF_INT
    GS_GetStat OFF_SPD
    movss    dword [rdx+OFF_COEF], xmm0
    mov      word  [rdx+OFF_COEF_IMPACT], si

    mov      rax, rbx
    pop      rbx
    ret


; DisplayStats
; Display the stats of a monster.
;
; Params:
;   rdi - address - address of the Stats structure
;
global DisplayStats
DisplayStats:
    push     rbx
    mov      rbx, rdi

    DS_DisplayLine vitMsg, 0
    DS_DisplayLine dexMsg, OFF_DEX
    DS_DisplayLine strMsg, OFF_STR
    DS_DisplayLine defMsg, OFF_DEF
    DS_DisplayLine intMsg, OFF_INT
    DS_DisplayLine spdMsg, OFF_SPD
    mov      rdi, coeffMsg
    call     PrintStringSlow

    movss    xmm0, dword [rbx+OFF_COEF]
    mov      si,   word [rbx+OFF_COEF_IMPACT]
    movss    xmm1, dword [growthBreaks]
    ucomiss  xmm0, xmm1
    jb       DS_Average

    DS_CmpGrowth 4, growthAb1Msg, growthBe1Msg
    DS_CmpGrowth 8, growthAb2Msg, growthBe2Msg
    DS_CmpGrowth 12, growthAb3Msg, growthBe3Msg
    DS_CmpGrowth 16, growthAb4Msg, growthBe4Msg
    DS_CmpGrowth 20, growthAb5Msg, growthBe5Msg
    DS_CmpGrowth 24, growthAb6Msg, growthBe6Msg
    DS_CmpGrowth 28, growthAb7Msg, growthBe7Msg
    DS_CmpGrowth 32, growthAb8Msg, growthBe8Msg
    DS_CmpGrowth 36, growthAb9Msg, growthBe9Msg

    cmp      si, UINT_16_MIDDLE
    jb       .PerfectNegative

    mov      rdi, growthPerfMsg
    jmp      DS_End

.PerfectNegative:
    mov      rdi, growthNullMsg
    jmp      DS_End

DS_Average:
    mov      rdi, growthAveMsg

DS_End:
    call     PrintStringSlow
    call     PrintNewLine

    pop      rbx
    ret


; GetHpMp
; Get the Hp and Mp of the monster based on it's vit/dex scores.
;
; Params:
;   rdi - address - address of the monster structure
;
; Returns:
;   rax - address - address of the monster structure
;
global GetHpMp
GetHpMp:
    push     rbx
    mov      rbx, rdi

    mov      rbx, qword [rdi+OFF_STATS]
    mov      eax, dword [rbx]
    movzx    ecx, byte [rdi+OFF_LVL] 
    xor      rdx, rdx
    mul      ecx
    mov      dword [rdi+OFF_MAX_HP+4], edx
    mov      dword [rdi+OFF_MAX_HP], eax
    mov      dword [rdi+OFF_CUR_HP+4], edx
    mov      dword [rdi+OFF_CUR_HP], eax

    mov      eax, dword [rbx+OFF_DEX]
    mul      ecx
    mov      dword [rdi+OFF_MAX_MP+4], edx
    mov      dword [rdi+OFF_MAX_MP], eax
    mov      dword [rdi+OFF_CUR_MP+4], edx
    mov      dword [rdi+OFF_CUR_MP], eax
    
    mov      rax, rbx
    pop      rbx
    ret


; GetXpRequired
; Get the xp required to lvl up given a lvl.
;
; Params:
;   dil - value - level
;
; Returns:
;   rax - value - xp required
;
global GetXpRequired
GetXpRequired:
    push     rbx                ; Xp required = Lvl*(100*(Lvl/2)) 
    movzx    ebx, dil           
    cvtsi2sd xmm0, rbx          ; Stores the value Lvl/2 in xmm2  
    mov      rbx, 2
    cvtsi2sd xmm1, rbx
    movsd    xmm2, xmm0
    divsd    xmm2, xmm1

    mov      rbx, 100           ; Stores the value 100*(Lvl/2) in xmm1 
    cvtsi2sd xmm1, rbx
    mulsd    xmm1, xmm2

    mulsd    xmm0, xmm1
    cvtsd2si rax, xmm0

    pop      rbx
    ret
