;------------
section .data

%include "../utils/constants.asm"
%include "towns_array.asm"

;------------
section .bss

extern PrintStringSlow
extern Malloc
extern NewStructNpc
extern NewStructTown

;------------
section .text

; TownInit
; Initialise a town.
;
; Vars:
;   rbp+16 - address - Game structure
;
; Params:
;   rdi - address - Game structure
;
; Returns:
;   rax - address - Game structure
;
global TownInit
TownInit:
    push    rbp
    mov     rbp, rsp
    push    rbx
    push    r12
    push    r13
    push    r14
    push    r15

    mov     qword [rbp+16], rdi
    movzx   eax, byte [rbp+17]                  ; rbx = People array for the Town constructor
    mov     r12, qword [townsArray+(rax*8)]     ; r12 = town init
    xor     r14, r14                            ; r13 = people init array 
    mov     r13, qword [r12+5]                  ; r14 = loop index 
    movzx   r15d, byte [r12+4]                  ; r15 = number of people 

    mov     rax, r15
    mov     rdi, 8
    xor     rdx, rdx
    mul     rdi
    mov     rdi, rax
    call    Malloc
    mov     rbx, rax

.Loop:
    mov     rax, qword [r13+(r14*8)] 
    mov     dl, byte [rax]
    movzx   edi, byte [rax+1]
    add     rax, 2
    mov     rsi, qword [rax+rdi]
    mov     rdi, rax
    call    NewStructNpc
    mov     qword [rbx+(r14*8)], rax
    inc     r14
    cmp     r14, r15
    jne     .Loop

    mov     dil, byte [r12]
    mov     sil, byte [r12+1]
    mov     dl, byte [r12+2]
    mov     r10b, byte [r12+3]
    mov     r9b, byte [r12+4]
    mov     r8, rbx
    call    NewStructTown

    mov     rdi, qword [rbp+16]
    mov     rsi, rax 
    call    TownMenu

    pop     r15
    pop     r14
    pop     r13
    pop     r12
    pop     rbx
    pop     rbp
    ret     8


; TownMenu
; Display the town menu.
;
; Params:
;   rdi - address - Game structure
;   rsi - address - Town structure
;
; Returns:
;   rax - address - Game structure
;
TownMenu:

    ret
