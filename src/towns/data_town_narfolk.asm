%include "../utils/constants.asm"

global narfolk  
narfolk db 0, 1, 0, 0, 3
        dq narfolkInitPeople 

narfolkInitPeople dq oldMan, youngBoy, woman

oldMan db 0, 8
       db "Old man", NULL
       db "You need to weaken a monster before using a catcher to increase your chances of catching it.", LF, NULL
       
youngBoy db 0, 10
         db "Young boy", NULL
         db "There are champions in gym all other the world, if you get all the badges you can go to the Wesnaca isles.", LF, NULL

woman db 0, 6
      db "Woman", NULL
      db "I think they sell map in Oshana, north of here.", LF, NULL
       

