extern narfolk
townsArray dq narfolk

choiceMsg1       db "1) ", 0
choiceMsg2       db "2) ", 0
choiceMsg3       db "3) ", 0
choiceMsg4       db "4) ", 0
choiceMsg5       db "5) ", 0
choiceMsg6       db "6) ", 0
choiceMsg7       db "7) ", 0
choiceMsg8       db "8) ", 0
choiceMsg9       db "9) ", 0
menuInnMsg       db "Inn", 0
menuMapMsg       db "Map", 0
menuTalkMsg      db "Talk to people", 0
menuSaveMsg      db "Save", 0
menuExitGameMsg  db "Exit game", 0
menuLeaveTownMsg db "Leave town", 0
menuStatusMsg    db "Status", 0
menuShopMsg      db "Shop", 0
menuGymMsg       db "Gym", 0

choiceMsgArray dq choiceMsg1, choiceMsg2, choiceMsg3, choiceMsg4, choiceMsg5, choiceMsg6, choiceMsg7, choiceMsg8, choiceMsg9
