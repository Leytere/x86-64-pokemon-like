;------------
section .data

%include "utils/constants.asm"

;------------
section .bss

extern GameStart
extern FreeStructGame

;------------
section .text

global _start
_start:
    nop
    call   GameStart

    mov    rdi, rax
    call   FreeStructGame

_last:
    mov    rax, SYS_EXIT
    mov    rdi, EXIT_SUCCESS
    syscall

