;------------
section .data

%include "../utils/constants.asm"

state  db 0

;------------
section .bss

extern CharacterCreation
extern TownInit

;------------
section .text

; GameStart
; Initialise the game.
;
; Returns:
;   rax - address - Game structure
; 
global GameStart
GameStart:
    ; call SaveCheck
    ; if save not valid:
    push   0
    call   CharacterCreation

    mov    rdi, TOWN_NARFOLK
    jmp    .TownInit

    ; Initialise game loop.
.GameLoop:
    cmp    byte [rax], TOWN_STATE
    je     .TownInit

.TownInit:
    call   TownInit
    jmp    .GameLoop

    ret
