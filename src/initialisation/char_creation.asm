;------------
section .data

%include "../utils/constants.asm"

initMsg       db "Type the name of your character (50 letters max): ", LF, LF, NULL
chooseMsg1    db "Welcome ", NULL
chooseMsg2    db "Choose your first monster:", LF, LF, "1) Slime", LF, "2) Dracky", LF, "3) Chimaera", LF, LF, NULL 
monsterExMsg1 db LF, "It's an adorable amorphic monster. Very affectious.", LF, LF, NULL
monsterExMsg2 db LF, "It's a shy monster, but it loves playing pranks on people and other monsters.", LF, LF, NULL
monsterExMsg3 db LF, "It's not the quieter monster in the lot, but it's a courageous one!", LF, LF, NULL
monsterExMenu db "1) Examine", LF, "2) Status", LF, "3) Take", LF, "4) Change", LF, LF, NULL
endMsg1       db "You took ", NULL
endMsg2       db "Great choice !", LF, "Now you can roam around the wold and train to become a master !", LF, NULL

;------------
section .bss

extern PrintStringSlow
extern PrintNewLine
extern GetRangedOption
extern PrintMonster
extern GetString
extern DisplayStats
extern NewStructGame
extern NewStructMonster
extern FreeStructMonster

;------------
section .text

; CharacterCreation
; Initialise the creation of a new character.
;
; Vars:
;   rbp+16 - bitmask - monsters to free
;
; Returns:
;   rax - address - Game structure
;
global CharacterCreation
CharacterCreation:
    push   rbp
    mov    rbp, rsp
    push   rbx
    push   r12
    push   r13
    push   r14
    push   r15

    mov    rdi, initMsg
    call   PrintStringSlow
    mov    rdi, 51
    call   GetString

    mov    rdi, rax
    xor    rsi, rsi
    xor    rdx, rdx
    xor    r10, r10
    xor    r8, r8
    xor    r9, r9
    call   NewStructGame
    mov    rbx, rax

    xor    rdi, rdi
    mov    rsi, 3
    xor    rdx, rdx
    call   NewStructMonster
    mov    r13, rax
    mov    rdi, 1
    mov    rsi, 3
    xor    rdx, rdx
    call   NewStructMonster
    mov    r14, rax
    mov    rdi, 2
    mov    rsi, 3
    xor    rdx, rdx
    call   NewStructMonster
    mov    r15, rax

; Choose monster messages
    mov    rdi, chooseMsg1
    call   PrintStringSlow

    mov    rdi, qword [rbx+OFF_GAME_NAME]
    call   PrintStringSlow

.MonsterChoice:
    call   PrintNewLine                  ; rbx  = address of Game
    mov    rdi, chooseMsg2               ; r12  = monster choice
    call   PrintStringSlow               ; r13  = monster 1 struct
                                         ; r14  = monster 2 struct 
    mov    dil, '1'                      ; r15  = monster 3 struct
    mov    sil, '3'
    push   0
    call   GetRangedOption
    cmp    al, '2'
    je     .SecondMonster
    cmp    al, '3'
    je     .ThirdMonster

.FirstMonster:
    mov    r12, r13
    mov    rdi, r13
    mov    byte [rbp+16], 00000110b
    mov    rsi, monsterExMsg1
    jmp    .CallMonsterChoice

.SecondMonster:
    mov    r12, r14
    mov    rdi, r14
    mov    byte [rbp+16], 00000101b
    mov    rsi, monsterExMsg2
    jmp    .CallMonsterChoice

.ThirdMonster:
    mov    r12, r15
    mov    rdi, r15
    mov    byte [rbp+16], 00000011b
    mov    rsi, monsterExMsg3

.CallMonsterChoice:
    call   MonsterChoice
    cmp    al, 1
    jne    .MonsterChoice

.EndCreation:
    mov    rax, qword [rbx+OFF_TEAM]
    mov    qword [rax], r12
    mov    rdi, endMsg1
    call   PrintStringSlow
    mov    rdi, qword [r12+OFF_MONSTER_NAME]
    call   PrintStringSlow
    call   PrintNewLine
    mov    rdi, endMsg2
    call   PrintStringSlow

    mov    r12b, byte [rbp+16]
    shr    r12b, 1
    jb     .FreeMonster1
.FreeLoop1:
    shr    r12b, 1
    jb     .FreeMonster2
.FreeLoop2:
    shr    r12b, 1
    jb     .FreeMonster3

.End:
    mov    rax, rbx
    pop    r15
    pop    r14
    pop    r13
    pop    r12
    pop    rbx
    pop    rbp
    ret    8

.FreeMonster1:
    mov    rdi, r13
    call   FreeStructMonster
    jmp    .FreeLoop1

.FreeMonster2:
    mov    rdi, r14
    call   FreeStructMonster
    jmp    .FreeLoop2

.FreeMonster3:
    mov    rdi, r15
    call    FreeStructMonster
    jmp     .End


; MonsterChoice
; Manage user interactions with a choosed monster.
;
; Params:
;   rdi - value - id of the monster
;   rsi - address - description message address
;
; Returns:
;   al  - bool    - is taking it
;
global MonsterChoice
MonsterChoice:
    push   rbx

    mov    rbx, rdi
    mov    rdi, rsi
    call   PrintStringSlow

.Menu:
    mov    rdi, monsterExMenu
    call   PrintStringSlow

    mov    dil, '1'
    mov    sil, '4'
    push   0
    call   GetRangedOption

    cmp    al, '1'
    je     .Examine
    cmp    al, '2'
    je     .Status
    cmp    al, '4'
    je     .Change

.Take:
    mov    al, 1

.End:
    pop    rbx
    ret 

.Examine:
    movzx  edi, byte [rbx]
    call   PrintMonster
    jmp    .Menu

.Change:
    mov    al, 0
    jmp    .End

.Status:
    call   PrintNewLine
    mov    rdi, qword [rbx+OFF_STATS]
    call   DisplayStats
    call   PrintNewLine
    jmp    .Menu

