;------------
section .data

%include "constants.asm"

;------------
section .bss

;------------
section .text

; Sleep
; Wait a certain amount of time.
;
; Params:
;   rdi - value - number of seconds to wait
;   rsi - value - number of nanoseconds to wait.
;
global Sleep
Sleep:
    push   rsi
    push   rdi

    mov    rdi, rsp
    mov    rax, SYS_NANOSLEEP
    xor    rsi, rsi
    syscall

    pop    rdi
    pop    rsi
    ret


; Malloc
; Allocate a given amount of bytes in memory.
;
; Params:
;   rdi - value - number of bytes to allocate
;
; Returns:
;   rax - address - address of the allocated memory
;
global Malloc
Malloc:
    mov    rsi, rdi
    mov    rax, SYS_MMAP
    xor    rdi, rdi
    mov    rdx, (PROT_READ | PROT_WRITE)
    mov    r10, (MAP_ANONYMOUS | MAP_PRIVATE)
    mov    r8, -1
    xor    r9, r9
    syscall

    cmp    rax, 0
    jl     Malloc

    ret


; Free
; Free a given amount of bytes in memory.
;
; Params:
;   rdi - address - address of the allocated memory.
;   rsi - value   - amount of bytes to deallocate.
;
global Free
Free:
    mov    rax, SYS_MUNMAP
    syscall

    ret
