%macro write 2
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, %1
    mov rdx, %2
    syscall
%endmacro

;------------
section .data

%include "constants.asm"
%include "monsters_id_to_path.asm"

gnErrMsg       db "You must at least enter one letter.", LF, NULL
ftbErrMsg      db "Error during file read", LF, NULL 
groErrEmptyMsg db "You must enter a choice.", LF, LF, NULL
groErrNIRMsg   db LF, "Your choice is invalid.", LF, LF, NULL
newLine        db LF, NULL

;------------
section .bss

extern Sleep
extern Malloc
extern Free

textBuffer resq 1000

;------------
section .text

global namesArray

; PrintString
; Print a NULL terminated string.
;
; Params:
;   rdi - address - string to be read
;   rsi - value - sleeping time between each character in nanoseconds
;
global PrintString
PrintString:
    push   rbx
    push   rdi
    push   rsi

    mov    rbx, rdi
.Loop:
    mov    rdi, 0
    pop    rsi
    call   Sleep
    push   rsi
    cmp    byte [rbx], NULL
    je     .End
    write  rbx, 1
    inc    rbx
    jmp    .Loop

.End:
    pop    rsi
    pop    rdi
    pop    rbx

    ret


; PrintStringSlow
; Calls PrintString with the SLEEP_HIGH constant.
;
; Params:
;   rdi - address - address of the null terminated string.
;
global PrintStringSlow
PrintStringSlow:
    mov    rsi, SLEEP_HIGH
    call   PrintString

    ret
    

; PrintStringFast
; Calls PrintString with the SLEEP_LOW constant.
;
; Params:
;   rdi - address - address of the null terminated string.
;
global PrintStringFast
PrintStringFast:
    mov    rsi, SLEEP_LOW
    call   PrintString

    ret


; PrintStringInstant
; Calls PrintString without delay.
;
; Params:
;   rdi - address - address of the null terminated string.
;
global PrintStringInstant
PrintStringInstant:
    xor    rsi, rsi
    call   PrintString

    ret


; PrintNewLine
; Print a new line.
;
global PrintNewLine
PrintNewLine:
    mov    rdi, newLine
    mov    rsi, SLEEP_HIGH
    call   PrintString

    ret


; PrintMonster
; Print an ascii art monster.
;
; Params:
;   rdi - value - id of the monster
;
global PrintMonster
PrintMonster:
    mov    rdi, qword [idsArray+(rdi*8)]
    mov    rax, SYS_OPEN
    mov    rsi, O_RDONLY
    syscall

    push   rax
    mov    rdi, rax
    call   FileToBuffer

    mov    rax, SYS_CLOSE
    pop    rdi
    syscall

    mov    rdi, textBuffer
    call   PrintStringFast
     
    ret


; FileToBuffer
; Place the content of a file into a buffer
;
; Params:
;   rdi - value - file descriptor
;
global FileToBuffer
FileToBuffer:
    push   rbx
    xor    rbx, rbx

.Loop:
    mov    rax, SYS_READ
    mov    rsi, textBuffer
    add    rsi, rbx
    mov    rdx, 1
    syscall
    inc    rbx

    cmp    rax, 0
    jl     .Err
    jne    .Loop

    dec    rbx
    mov    byte [textBuffer+rbx], NULL

.End
    pop    rbx
    ret

.Err:
    mov    rdi, ftbErrMsg
    mov    rsi, SLEEP_HIGH
    call   PrintString
    jmp    .End


; GetRangedOption
; Get user input between an ascii range.
;
; Vars:
;   rbp+16 - byte - optionBuffer
;   rbp+17 - byte - charBuffer
;
; Params:
;   dil - value - min range unsigned
;   sil - value - max range unsigned
;
; Returns:
;   al - value - the selected option
;
global GetRangedOption
GetRangedOption:
    push   rbp
    mov    rbp, rsp

    push   rbx
    push   rsi
    push   rdi

.LoopInit
    xor    rbx, rbx
    mov    byte [rbp+16], 0

.Loop:
    mov    rax, SYS_READ
    mov    rdi, STDIN
    mov    rsi, rbp
    add    rsi, 17
    mov    rdx, 1
    syscall

    mov    al, byte [rbp+17]
    cmp    al, LF
    je     .Check
    inc    rbx
    cmp    rbx, 1
    jne    .Loop
    mov    byte [rbp+16], al
    jmp    .Loop

.Check
    mov    al, byte [rbp+16] 
    cmp    al, 0
    je     .ErrEmpty
    pop    rdi
    cmp    al, dil
    jb     .ErrBelowRange
    pop    rsi
    cmp    al, sil
    ja     .ErrAboveRange
    
    pop    rbx
    pop    rbp
    ret    8

.ErrAboveRange:
    push    rsi
.ErrBelowRange:
    push    rdi
    mov     rdi, groErrNIRMsg
    call    PrintStringFast
    jmp     .LoopInit

.ErrEmpty:
    mov     rdi, groErrEmptyMsg
    call    PrintStringFast
    jmp     .LoopInit

    
; PrintUnsignedNumber
; Print a 64bit unsigned number.
;
; Vars:
;   rbp+16 - byte - ascii number
;
; Params:
;   rdi - value - number to be printed.
;
global PrintUnsignedNumber
PrintUnsignedNumber:
    push   rbp
    mov    rbp, rsp
    push   rbx
    xor    rbx, rbx

    mov    rax, rdi
.PushLoop:
    xor    rdx, rdx
    mov    rcx, 10
    div    rcx
    push   rdx
    inc    rbx
    cmp    rax, 0
    jne    .PushLoop

.CallWrite:
    pop    rdi
    add    rdi, 30h
    mov    byte [rbp+16], dil
    mov    rsi, rbp
    add    rsi, 16
    write  rsi, 1
    dec    rbx
    cmp    rbx, 0
    jne    .CallWrite

    pop    rbx
    pop    rbp
    ret    8

; GetString
; Get a string from the user.
;
; Vars:
;   rbp+16 - byte - charBuffer
;
; Params:
;   rdi - max bytes (counting the terminating NULL) 
;
; Returns:
;   rax - address - address of the string
;
global GetString
GetString:
    push   rbp
    mov    rbp, rsp

    push   rbx
    xor    rbx, rbx
    push   r12
    mov    r12, rdi
    
    call   Malloc
    mov    r8, rax
    mov    byte [r8], 0

.Loop:
    mov    rax, SYS_READ
    mov    rdi, STDIN
    mov    rsi, rbp
    add    rsi, 16
    mov    rdx, 1
    syscall

    cmp    byte [rbp+16], LF
    je     .Check

    mov    al, byte [rbp+16]
    mov    byte [r8+rbx], al

    inc    rbx
    dec    r12
    cmp    r12, 0
    jne    .Loop

.Check:
    cmp    byte [r8], 0
    je     .Err
    mov    byte [r8+rbx], 0
    inc    rbx
    dec    r12

    cmp    rcx, 0
    je     .End

    mov    rdi, rbx
    mov    rsi, r12
    call   Free

.End:
    mov    rax, r8

    pop    r12
    pop    rbx
    pop    rbp
    ret

.Err:
    mov    rdi, gnErrMsg
    call   PrintStringSlow
    add    r12, rbx
    xor    rbx, rbx
    jmp    .Loop


; FreeString
; Free a string.
;
; Params
;   rdi - address - string 
;
global FreeString
FreeString:
    push   rbx
    mov    rbx, rdi
    xor    rcx, rcx

.CountLoop:
    mov    al, byte [rbx+rcx]
    inc    rcx
    cmp    al, 0
    jne    .CountLoop
    
    mov    rdi, rbx
    mov    rsi, rcx
    call   Free

    pop    rbx
    ret
