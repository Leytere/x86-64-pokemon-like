slime            db "monsters/slime.ascii", 0              ; 00 - Slime
chimaera         db "monsters/chimaera.ascii", 0           ; 01 - Chimaera
dracky           db "monsters/dracky.ascii", 0             ; 02 - Dracky
anchorman        db "monsters/anchorman.ascii", 0          ; 03 - Anchorman
archdemon        db "monsters/archdemon.ascii", 0          ; 04 - Archdemon
argonLizard      db "monsters/argon_lizard.ascii", 0       ; 05 - Argon Lizard
bagOLaughs       db "monsters/bag_o_laughs.ascii", 0       ; 06 - Bag'O Laughs
beetleboy        db "monsters/beetleboy.ascii", 0          ; 07 - Beetleboy
boneBaron        db "monsters/bone_baron.ascii", 0         ; 08 - Bone Baron
bossTroll        db "monsters/boss_troll.ascii", 0         ; 09 - Boss Troll
bubbleSlime      db "monsters/bubble_slime.ascii", 0       ; 10 - Bubble Slime
bullflinch       db "monsters/bullflinch.ascii", 0         ; 11 - Bullflinch
cannibox         db "monsters/cannibox.ascii", 0           ; 12 - Cannibox
capsichum        db "monsters/capsichum.ascii", 0          ; 13 - Capsichum
chainine         db "monsters/chainine.ascii", 0           ; 14 - Chainine
cockateer        db "monsters/cockateer.ascii", 0          ; 15 - Cockateer
dancingDevil     db "monsters/dancing_devil.ascii", 0      ; 16 - Dancing Devil
diemon           db "monsters/diemon.ascii", 0             ; 17 - Diemon
dingaling        db "monsters/dingaling.ascii", 0          ; 18 - Dingaling
dragonthorn      db "monsters/dragonthorn.ascii", 0        ; 19 - Dragonthorn
dullahan         db "monsters/dullahan.ascii", 0           ; 20 - Dullahan
fallenPriest     db "monsters/fallen_priest.ascii", 0      ; 21 - Fallen Priest
fencingFox       db "monsters/fencing_fox.ascii", 0        ; 22 - Fencing Fox
firespirit       db "monsters/firespirit.ascii", 0         ; 23 - Firespirit
flyguy           db "monsters/flyguy.ascii", 0             ; 24 - Flyguy
frogface         db "monsters/frogface.ascii", 0           ; 25 - Frogface
funghoul         db "monsters/funghoul.ascii", 0           ; 26 - Funghoul
ghost            db "monsters/ghost.ascii", 0              ; 27 - Ghost
giantMoth        db "monsters/giant_moth.ascii", 0         ; 28 - Giant Moth
gigantes         db "monsters/gigantes.ascii", 0           ; 29 - Gigantes
golem            db "monsters/golem.ascii", 0              ; 30 - Golem
goodybag         db "monsters/goodybag.ascii", 0           ; 31 - Goodybag
gorerilla        db "monsters/gorerilla.ascii", 0          ; 32 - Gorerilla
greatArgonLizard db "monsters/great_argon_lizard.ascii", 0 ; 33 - Great Argon Lizard
greatSabercat    db "monsters/great_sabercat.ascii", 0     ; 34 - Great Sabercat
hacksaurus       db "monsters/hacksaurus.ascii", 0         ; 35 - Hacksaurus
hadesCondor      db "monsters/hades_condor.ascii", 0       ; 36 - Hades Condor
hammerhood       db "monsters/hammerhood.ascii", 0         ; 37 - Hammerhood
healslime        db "monsters/healslime.ascii", 0          ; 38 - Healslime
heligator        db "monsters/heligator.ascii", 0          ; 39 - Heligator
hellhound        db "monsters/hellhound.ascii", 0          ; 40 - Hellhound
imp              db "monsters/imp.ascii", 0                ; 41 - Imp
jailcat          db "monsters/jailcat.ascii", 0            ; 42 - Jailcat
jargon           db "monsters/jargon.ascii", 0             ; 43 - Jargon
khalamariKid     db "monsters/khalamari_kid.ascii", 0      ; 44 - Khalamari Kid
killingMachine   db "monsters/killing_machine.ascii", 0    ; 45 - Killing Machine
kingKelp         db "monsters/king_kelp.ascii", 0          ; 46 - King Kelp
kingSlime        db "monsters/king_slime.ascii", 0         ; 47 - King Slime
lips             db "monsters/lips.ascii", 0               ; 48 - Lips
livingStatue     db "monsters/living_statue.ascii", 0      ; 49 - Living Statue
mechaMynah       db "monsters/mecha_mynah.ascii", 0        ; 50 - Mecha Mynah
mischievousMole  db "monsters/mischievous_mole.ascii", 0   ; 51 - Mischievous Mole
mudMannequin     db "monsters/mud_mannequin.ascii", 0      ; 52 - Mud Mannequin
muddyHand        db "monsters/muddy_hand.ascii", 0         ; 53 - Muddy Hand
mummyBoy         db "monsters/mummy_boy.ascii", 0          ; 54 - Mummy Boy
octavianSentry   db "monsters/octavian_sentry.ascii", 0    ; 55 - Octavian Sentry
orc              db "monsters/orc.ascii", 0                ; 56 - Orc
panther          db "monsters/panther.ascii", 0            ; 57 - Panther
phantomFencer    db "monsters/phantom_fencer.ascii", 0     ; 58 - Phantom Fencer
platypunk        db "monsters/platypunk.ascii", 0          ; 59 - Platypunk
puppeteer        db "monsters/puppeteer.ascii", 0          ; 60 - Puppeteer
restlessArmour   db "monsters/restless_armour.ascii", 0    ; 61 - Restless Armour
riptide          db "monsters/riptide.ascii", 0            ; 62 - Riptide
rockbomb         db "monsters/rockbomb.ascii", 0           ; 63 - Rockbomb
satyr            db "monsters/satyr.ascii", 0              ; 64 - Satyr
scorpion         db "monsters/scorpion.ascii", 0           ; 65 - Scorpion
seeUrchin        db "monsters/see_urchin.ascii", 0         ; 66 - See Urchin
shadow           db "monsters/shadow.ascii", 0             ; 67 - Shadow
shellSlime       db "monsters/shell_slime.ascii", 0        ; 68 - Shell Slime
skeleton         db "monsters/skeleton.ascii", 0           ; 69 - Skeleton
skipper          db "monsters/skipper.ascii", 0            ; 70 - Skipper
slimeKnight      db "monsters/slime_knight.ascii", 0       ; 71 - Slime Knight
soulspawn        db "monsters/soulspawn.ascii", 0          ; 72 - Soulspawn
spikedHare       db "monsters/spiked_hare.ascii", 0        ; 73 - Spiked Hare
spitnik          db "monsters/spitnik.ascii", 0            ; 74 - Spitnik
treeface         db "monsters/treeface.ascii", 0           ; 75 - Treeface
wildBoarfish     db "monsters/wild_boarfish.ascii", 0      ; 76 - Wild Boarfish
winky            db "monsters/winky.ascii", 0              ; 77 - Winky

idsArray dq slime, chimaera, dracky, anchorman, archdemon, argonLizard, bagOLaughs, beetleboy, boneBaron, bossTroll, bubbleSlime, bullflinch, cannibox
         dq capsichum, chainine, cockateer, dancingDevil, diemon, dingaling, dragonthorn, dullahan, fallenPriest, fencingFox, firespirit, flyguy, frogface, funghoul
         dq ghost, giantMoth, gigantes, golem, goodybag, gorerilla, greatArgonLizard, greatSabercat, hacksaurus, hadesCondor, hammerhood, healslime
         dq heligator, hellhound, imp, jailcat, jargon, khalamariKid, killingMachine, kingKelp, kingSlime, lips, livingStatue, mechaMynah, mischievousMole
         dq mudMannequin, muddyHand, mummyBoy, octavianSentry, orc, panther, phantomFencer, platypunk, puppeteer, restlessArmour, riptide, rockbomb, satyr
         dq scorpion, seeUrchin, shadow, shellSlime, skeleton, skipper, slimeKnight, soulspawn, spikedHare, spitnik, treeface, wildBoarfish, winky

nSlime            db "Slime", 0
nChimaera         db "Chimaera", 0
nDracky           db "Dracky", 0
nAnchorman        db "Anchorman", 0
nArchdemon        db "Archdemon", 0
nArgonLizard      db "Argon Lizard", 0
nBagOLaughs       db "Bag'O Laughs", 0
nBeetleboy        db "Beetleboy", 0
nBoneBaron        db "Bone Baron", 0
nBossTroll        db "Boss Troll", 0
nBubbleSlime      db "Bubble Slime", 0
nBullflinch       db "Bullflinch", 0
nCannibox         db "Cannibox", 0
nCapsichum        db "Capsichum", 0
nChainine         db "Chainine", 0
nCockateer        db "Cockateer", 0
nDancingDevil     db "Dancing Devil", 0
nDiemon           db "Diemon", 0
nDingaling        db "Dingaling", 0
nDragonthorn      db "Dragonthorn", 0
nDullahan         db "Dullahan", 0
nFallenPriest     db "Fallen Priest", 0
nFencingFox       db "Fencing Fox", 0
nFirespirit       db "Firespirit", 0
nFlyguy           db "Flyguy", 0
nFrogface         db "Frogface", 0
nFunghoul         db "Funghoul", 0
nGhost            db "Ghost", 0
nGiantMoth        db "Giant Moth", 0
nGigantes         db "Gigantes", 0
nGolem            db "Golem", 0
nGoodybag         db "Goodybag", 0
nGorerilla        db "Gorerilla", 0
nGreatArgonLizard db "Great Argon Lizard", 0
nGreatSabercat    db "Great Sabercat", 0
nHacksaurus       db "Hacksaurus", 0
nHadesCondor      db "Hades Condor", 0
nHammerhood       db "Hammerhood", 0
nHealslime        db "Healslime", 0
nHeligator        db "Heligator", 0
nHellhound        db "Hellhound", 0
nImp              db "Imp", 0
nJailcat          db "Jailcat", 0
nJargon           db "Jargon", 0
nKhalamariKid     db "Khalamari Kid", 0
nKillingMachine   db "Killing Machine", 0
nKingKelp         db "King Kelp", 0
nKingSlime        db "King Slime", 0
nLips             db "Lips", 0
nLivingStatue     db "Living Statue", 0
nMechaMynah       db "Mecha Mynah", 0
nMischievousMole  db "Mischievous Mole", 0
nMudMannequin     db "Mud Mannequin", 0
nMuddyHand        db "Muddy Hand", 0
nMummyBoy         db "Mummy Boy", 0
nOctavianSentry   db "Octavian Sentry", 0
nOrc              db "Orc", 0
nPanther          db "Panther", 0
nPhantomFencer    db "Phantom Fencer", 0
nPlatypunk        db "Platypunk", 0
nPuppeteer        db "Puppeteer", 0
nRestlessArmour   db "Restless Armour", 0
nRiptide          db "Riptide", 0
nRockbomb         db "Rockbomb", 0
nSatyr            db "Satyr", 0
nScorpion         db "Scorpion", 0
nSeeUrchin        db "See Urchin", 0
nShadow           db "Shadow", 0
nShellSlime       db "ShellSlime", 0
nSkeleton         db "Skeleton", 0
nSkipper          db "Skipper", 0
nSlimeKnight      db "Slime Knight", 0
nSoulspawn        db "Soulspawn", 0
nSpikedHare       db "Spiked Hare", 0
nSpitnik          db "Spitnik", 0
nTreeface         db "Treeface", 0
nWildBoarfish     db "Wild Boarfish", 0
nWinky            db "Winky", 0

namesArray dq nSlime, nChimaera, nDracky, nAnchorman, nArchdemon, nArgonLizard, nBagOLaughs, nBeetleboy, nBoneBaron, nBossTroll, nBubbleSlime, nBullflinch, nCannibox
           dq nCapsichum, nChainine, nCockateer, nDancingDevil, nDiemon, nDingaling, nDragonthorn, nDullahan, nFallenPriest, nFencingFox, nFirespirit, nFlyguy, nFrogface 
           dq nFunghoul, nGhost, nGiantMoth, nGigantes, nGolem, nGoodybag, nGorerilla, nGreatArgonLizard, nGreatSabercat, nHacksaurus, nHadesCondor, nHammerhood, nHealslime
           dq nHeligator, nHellhound, nImp, nJailcat, nJargon, nKhalamariKid, nKillingMachine, nKingKelp, nKingSlime, nLips, nLivingStatue, nMechaMynah, nMischievousMole
           dq nMudMannequin, nMuddyHand, nMummyBoy, nOctavianSentry, nOrc, nPanther, nPhantomFencer, nPlatypunk, nPuppeteer, nRestlessArmour, nRiptide, nRockbomb, nSatyr
           dq nScorpion, nSeeUrchin, nShadow, nShellSlime, nSkeleton, nSkipper, nSlimeKnight, nSoulspawn, nSpikedHare, nSpitnik, nTreeface, nWildBoarfish, nWinky
