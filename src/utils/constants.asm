EXIT_SUCCESS      equ 0

STDIN             equ 0
STDOUT            equ 1
STRLEN            equ 11

SYS_WRITE         equ 1
SYS_EXIT          equ 60
SYS_READ          equ 0
SYS_OPEN          equ 2
SYS_CLOSE         equ 3
SYS_NANOSLEEP     equ 35
SYS_MMAP          equ 9
SYS_MUNMAP        equ 11 

O_RDONLY          equ 0
PROT_READ         equ 1
PROT_WRITE        equ 2
MAP_ANONYMOUS     equ 0x20
MAP_PRIVATE       equ 0x02

NULL              equ 0
LF                equ 10
UINT_16_MIDDLE    equ 32767 
UINT_16_MAX       equ 65535
UINT_16_MAXR_1000 equ 64999
UINT_16_R1000_DIV equ 65

EXIT_STATE        equ 0
TOWN_STATE        equ 1

TOWN_NARFOLK      equ 0

SLEEP_HIGH        equ 20000000
SLEEP_LOW         equ 100000

OFF_GAME_NAME     equ 8
OFF_INVENTORY     equ 16
OFF_MISC          equ 24
OFF_TEAM          equ 32
OFF_STORAGE       equ 40
OFF_DEX           equ 4
OFF_STR           equ 8
OFF_DEF           equ 12
OFF_INT           equ 16
OFF_SPD           equ 20
OFF_COEF          equ 24
OFF_COEF_IMPACT   equ 28
OFF_LVL           equ 1
OFF_MONSTER_NAME  equ 2
OFF_STATS         equ 10
OFF_MAX_HP        equ 18
OFF_CUR_HP        equ 26
OFF_MAX_MP        equ 34
OFF_CUR_MP        equ 42
OFF_XP_TO_LVL     equ 50
OFF_CUR_XP        equ 58
OFF_ITEM_NAME     equ 1
OFF_QUANTITY      equ 9
OFF_PRICE         equ 10
OFF_IS_SELLABLE   equ 18
OFF_DESCRIPTION   equ 19 
OFF_EFFECT_ID     equ 27
OFF_ITEM_POWER    equ 28
OFF_MAP           equ 8
OFF_BADGES        equ 9
OFF_FIRST_EVENTS  equ 17
OFF_INN           equ 1
OFF_SHOP          equ 2
OFF_MAP_POSITION  equ 3
OFF_LIST_PEOPLE   equ 4
OFF_NUMBER_PEOPLE equ 12
OFF_NPC_MESSAGE   equ 8
OFF_NPC_EVENT_ID  equ 16

STATS_SIZE        equ 30
MONSTER_SIZE      equ 66
GAME_SIZE         equ 48
STORAGE_BOX_SIZE  equ 160
STORAGE_SIZE      equ 160
TEAM_SIZE         equ 48
INVENTORY_SIZE    equ 800
ITEM_SIZE         equ 31 
MISC_SIZE         equ 25
TOWN_SIZE         equ 13
NPC_SIZE          equ 17
