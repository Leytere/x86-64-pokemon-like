;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free
extern FreeStructMonster

;------------
section .text

; NewStructStorageBox
; StorageBox constructor.
;
; Params:
;   rdi - address - address of the monsters pointer array | null
;
; Returns:
;   rax - address - address of the new StorageBox
;
global NewStructStorageBox
NewStructStorageBox:
    push   rdi
    mov    rdi, STORAGE_BOX_SIZE
    call   Malloc
    pop    rdi

    xor    rcx, rcx
    cmp    rdi, 0
    jne    .FillWithArray

.ZeroInit:
    mov    qword [rax+(rcx*8)], 0
    inc    rcx
    cmp    rcx, 20
    jne    .ZeroInit
    jmp    .End

.FillWithArray:
    mov    rsi, qword [rdi+(rcx*8)]
    mov    qword [rax+(rcx*8)], rsi
    inc    rcx
    cmp    rcx, 20
    jne    .FillWithArray

    push   rax
    mov    rsi, STORAGE_BOX_SIZE
    call   Free
    pop    rax

.End:
    ret


; FreeStructStorageBox
; StorageBox destructor.
;
; Params:
;   rdi - address - address of the StorageBox
;
global FreeStructStorageBox
FreeStructStorageBox:
    push   rbx
    push   r12
    mov    rbx, rdi
    xor    r12, r12

.Loop:
    cmp    r12, 20
    je     .EndLoop
    cmp    qword [rbx+(r12*8)], 0
    jne    .FreeMonster
    inc    r12
    jmp    .Loop

.EndLoop:
    mov    rdi, rbx
    mov    rsi, STORAGE_BOX_SIZE
    call   Free

    pop    r12
    pop    rbx
    ret

.FreeMonster:
    mov    rdi, qword [rbx+(r12*8)]
    call   FreeStructMonster
    inc    r12
    jmp    .Loop
