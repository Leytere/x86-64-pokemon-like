;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free
extern FreeStructNpc

;------------
section .text

; NewStructTown
; Town constructor.
;
; Params:
;   dil  - bool    - have gym
;   sil  - bool    - have inn
;   dl   - bool    - have shop
;   r10b - value   - map position
;   r8   - address - array of npc | null
;   r9l  - value   - number of npc
;
; Returns:
;   rax - address - new Town structure
;
global NewStructTown
NewStructTown:
    push   rbx
    push   r9
    push   r8
    push   r10
    push   rbx
    push   rsi
    push   rdi

    mov    rdi, TOWN_SIZE
    call   Malloc
    mov    rbx, rax

    pop    rdi
    mov    byte [rbx], dil
    pop    rdi
    mov    byte [rbx+OFF_INN], dil
    pop    rdi
    mov    byte [rbx+OFF_SHOP], dil
    pop    rdi
    mov    byte [rbx+OFF_MAP_POSITION], dil
    pop    rdi
    mov    qword [rbx+OFF_LIST_PEOPLE], rdi
    pop    rdi
    mov    byte [rbx+OFF_NUMBER_PEOPLE], dil

    mov    rax, rbx

    pop    rbx
    ret


; FreeStructTown
; Free a Town structure.
;
; Params:
;   rdi - address - Town structure
;
global FreeStructTown
FreeStructTown:
    push   rbx
    push   r12
    push   r13
    push   r14

    xor    r14, r14
    mov    r13, rdi                                 ; rbx = address array npc
    mov    rbx, qword [r13+OFF_LIST_PEOPLE]         ; r12 = number of people
    movzx  r12d, byte [r13+OFF_NUMBER_PEOPLE]       ; r13 = address of Town
                                                    ; r14 = array offset
.Loop:
    mov    rdi, qword [rbx+(r14*8)]
    call   FreeStructNpc
    inc    r14
    cmp    r14, r12
    jne    .Loop

    xor    rdx, rdx
    mov    rax, 8
    mul    r12
    mov    rsi, rax
    mov    rdi, rbx
    call   Free

    pop    r14
    pop    r13
    pop    r12
    pop    rbx
    ret







