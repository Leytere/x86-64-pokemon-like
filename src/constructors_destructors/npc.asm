;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free

;------------
section .text

; NewStructNpc
; Npc constructor.
;
; Params:
;   rdi - address - name 
;   rsi - address - message
;   dl  - value   - event id
;
; Returns:
;   rax - address - new Npc structure
;
global NewStructNpc
NewStructNpc:
    push   rdx
    push   rsi
    push   rdi

    mov    rax, NPC_SIZE
    call   Malloc

    pop    rdi
    mov    qword [rax], rdi
    pop    rdi
    mov    qword [rax+OFF_NPC_MESSAGE], rdi
    pop    rdi
    mov    byte [rax+OFF_NPC_EVENT_ID], dil

    ret

; FreeStructNpc
; Npc destructor.
;
; Params:
;   rdi - address - Npc structure
;
global FreeStructNpc
FreeStructNpc:
    mov    rsi, NPC_SIZE
    call   Free

    ret
