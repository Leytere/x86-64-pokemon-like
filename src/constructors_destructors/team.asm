;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free
extern FreeStructMonster

;------------
section .text

; NewStructTeam
; Team constructor.
;
; Params:
;   rdi - address - monsters array | null
;
; Returns:
;   rax - address - address of the new Team
;
global NewStructTeam
NewStructTeam:
    push   rdi
    mov    rdi, TEAM_SIZE
    call   Malloc
    pop    rdi
    xor    rcx, rcx

    cmp    rdi, 0
    je     .ZeroFill

.Loop:
    cmp    qword [rdi+(rcx*8)], 0
    je     .ZeroFill
    mov    rsi, qword [rdi+(rcx*8)]
    mov    qword [rax+(rcx*8)], rsi
    inc    rcx
    cmp    rcx, 6
    jne    .Loop
    jmp    .End

.ZeroFill:
    mov    qword [rax+(rcx*8)], 0
    inc    rcx
    cmp    rcx, 6
    jne    .ZeroFill

.End:
    push   rax
    mov    rsi, TEAM_SIZE
    call   Free

    pop    rax
    ret


; FreeStructTeam
; Team destructor.
;
; Params:
;   rdi - address - address of the Team structure
;
global FreeStructTeam
FreeStructTeam:
    push   rbx
    push   r12
    mov    rbx, rdi
    xor    r12, r12

.Loop:
    cmp    qword [rbx+(r12*8)], 0
    je     .End
    mov    rdi, qword [rbx+(r12*8)]
    call   FreeStructMonster
    inc    r12
    cmp    r12, 6
    jne    .Loop

.End:
    mov    rdi, rbx
    mov    rsi, TEAM_SIZE
    call   Free

    pop    r12
    pop    rbx
    ret
