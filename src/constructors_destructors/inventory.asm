;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free
extern FreeStructItem

;------------
section .text

; NewStructInventory
; Inventory constructor.
;
; Params:
;   rdi - address - address of the item array | null
;
; Returns:
;   rax - address - address of the new Inventory
;
global NewStructInventory
NewStructInventory:
    push   rbx
    push   r12
    push   r13

    mov    r12, rdi                    ; rbx = address Inventory struct
    mov    rdi, INVENTORY_SIZE         ; r12 = address inventory array
    call   Malloc                      ; r13 = index
    mov    rbx, rax

    xor    r13, r13
    cmp    r12, 0
    jne    .FillWithArray

.ZeroFill:
    mov    qword [rbx+(r13*8)], 0
    inc    r13
    cmp    r13, 100
    jne    .ZeroFill
    jmp    .End

.FillWithArray:
    mov    rax, qword [r12+(r13*8)]
    mov    qword [rbx+(r13*8)], rax
    inc    r13
    cmp    r13, 100
    jne    .FillWithArray

    mov    rdi, r12
    mov    rsi, INVENTORY_SIZE
    call   Free
     
.End:
    pop    r13
    pop    r12
    pop    rbx
    ret


; FreeStructInventory
; Inventory destructor.
;
; Params:
;   rdi - address - address of the Inventory structure
;
global FreeStructInventory
FreeStructInventory:
    push   rbx
    push   r12
    mov    rbx, rdi
    xor    r12, r12

.Loop:
    mov    rdi, qword [rbx+(r12*8)]
    inc    r12
    cmp    rdi, 0
    jne    .FreeItem
    cmp    r12, 100 
    jne    .Loop

    mov    rdi, rbx
    mov    rsi, INVENTORY_SIZE
    call   Free

    pop    r12
    pop    rbx
    ret

.FreeItem:
    call   FreeStructItem
    jmp    .Loop
    
