;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free

;------------
section .text

; NewStructMisc
; Misc constructor.
;
; Params:
;   rdi - value   - gold
;   sil - bool    - have map
;   rdx - bitmask - badges
;   r10 - bitmask - first time events
;   
; Returns:
;   rax - address - address of the new Misc
;
global NewStructMisc
NewStructMisc:
    push   r10
    push   rdx
    push   rsi
    push   rdi
    mov    rdi, MISC_SIZE
    call   Malloc

    pop    rdi
    mov    qword [rax], rdi
    pop    rdi
    mov    byte [rax+OFF_MAP], dil
    pop    rdi
    mov    qword [rax+OFF_BADGES], rdi
    pop    rdi
    mov    qword [rax+OFF_FIRST_EVENTS], rdi

    ret

; FreeStructMisc
; Misc destructor.
;
; Params:
;   rdi - address - address of the Misc
;
global FreeStructMisc
FreeStructMisc:
    mov    rsi, MISC_SIZE
    call   Free

    ret
