;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free
extern NewStructStorageBox
extern FreeStructStorageBox

;------------
section .text

; NewStructStorage
; Storage constructor.
;
; Params:
;   rdi - address - address of the StorageBox array, must be a valid 20 qwords array | null
;
; Returns:
;   rax - address - address of the new Storage
;
global NewStructStorage
NewStructStorage:
    push   rbx
    push   r12
    push   r13

    mov    rbx, rdi                        ; rbx - address of the StorageBox array
    mov    rdi, STORAGE_SIZE               ; r12 - Storage offset
    call   Malloc                          ; r13 - Storage address
    mov    r13, rax

    xor    r12, r12 
    cmp    rbx, 0
    jne    .FillWithArray

.ZeroInit:
    xor    rdi, rdi
    call   NewStructStorageBox
    mov    qword [r13+(r12*8)], rax
    inc    r12
    cmp    r12, 20
    jne    .ZeroInit
    jmp    .End

.FillWithArray:
    mov    rdi, qword [rbx+(r12*8)]
    mov    qword [r13+(r12*8)], rdi
    inc    r12
    cmp    r12, 20
    jne    .FillWithArray

    mov    rdi, rbx
    mov    rsi, STORAGE_SIZE
    call   Free

.End:
    mov    rax, r13
    pop    r13
    pop    r12
    pop    rbx
    ret


; FreeStructStorage
; Storage destructor.
;
; Params:
;   rdi - address - address of the storage
;
global FreeStructStorage
FreeStructStorage:
    push   rbx
    push   r12
    mov    rbx, rdi
    xor    r12, r12

.Loop:
    cmp    r12, 20
    je     .EndLoop
    cmp    qword [rbx+(r12*8)], 0
    jne    .FreeStorageBox
    inc    r12
    jmp    .Loop

.EndLoop:
    mov    rdi, rbx
    mov    rsi, STORAGE_SIZE
    call   Free

    pop    r12
    pop    rbx
    ret

.FreeStorageBox:
    mov    rdi, qword [rbx+(r12*8)]
    call   FreeStructStorageBox
    inc    r12
    jmp    .Loop
