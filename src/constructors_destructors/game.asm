;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free
extern NewStructMisc
extern NewStructInventory
extern NewStructStorage
extern NewStructTeam
extern FreeString
extern FreeStructTeam
extern FreeStructStorage
extern FreeStructInventory
extern FreeStructMisc

;------------
section .text

; NewStructGame
; Game constructor, if a parameter is null, the attribute structure will be empty.
;
; Params:
;   rdi - address - player name
;   rsi - address - Inventory struct | null
;   rdx - address - Misc struct | null
;   r10 - address - Team struct | null
;   r8  - address - Storage struct | null 
;   r9  - value   - game state
;
; Returns:
;   rax - address - new Game structure
;
global NewStructGame
NewStructGame:
    push   rbx
    push   r8
    push   r10
    push   rdx
    push   rsi
    push   rdi
    push   r9

    mov    rdi, GAME_SIZE
    call   Malloc
    mov    rbx, rax

    pop    rdi
    mov    qword [rbx], rdi

    pop    rdi
    mov    qword [rbx+OFF_GAME_NAME], rdi

    pop    rax
    cmp    rax, 0
    je     .InventoryNull

.PlaceInventory;
    mov    qword [rbx+OFF_INVENTORY], rax

    pop    rax
    cmp    rax, 0
    je     .MiscNull

.PlaceMisc:
    mov    qword [rbx+OFF_MISC], rax

    pop    rax
    cmp    rax, 0
    je     .TeamNull

.PlaceTeam:
    mov    qword [rbx+OFF_TEAM], rax

    pop    rax
    cmp    rax, 0
    je     .StorageNull

.PlaceStorage:
    mov    qword [rbx+OFF_STORAGE], rax

    mov    rax, rbx
    pop    rbx
    ret


.InventoryNull:
    xor    rdi, rdi
    call   NewStructInventory
    jmp    .PlaceInventory

.MiscNull:
    xor    rdi, rdi
    xor    rsi, rsi
    xor    rdx, rdx
    xor    r10, r10
    call   NewStructMisc
    jmp    .PlaceMisc

.TeamNull:
    xor    rdi, rdi
    call   NewStructTeam
    jmp    .PlaceTeam

.StorageNull:
    xor    rdi, rdi
    call   NewStructStorage
    jmp    .PlaceStorage


; FreeStructGame
; Game destructor.
;
; Params:
;   rdi - address - Game structure
;
global FreeStructGame
FreeStructGame:
    push   rbx
    mov    rbx, rdi

    mov    rdi, qword [rbx]
    call   FreeString
    mov    rdi, qword [rbx+OFF_INVENTORY]
    call   FreeStructInventory
    mov    rdi, qword [rbx+OFF_MISC]
    call   FreeStructMisc
    mov    rdi, qword [rbx+OFF_TEAM]
    call   FreeStructTeam
    mov    rdi, qword [rbx+OFF_STORAGE]
    call   FreeStructStorage

    mov    rdi, rbx
    mov    rsi, GAME_SIZE
    call   Free

    pop    rbx
    ret











