;------------
section .data

%include "../utils/constants.asm"

;------------
section .bss

extern Malloc
extern Free

;------------
section .text

; NewStructItem
; Item constructor.
;
; Params:
;   dil    - value   - id of the item
;   rsi    - address - address of name string
;   dl     - value   - quantity
;   r10    - value   - price
;   r8b    - bool    - is sellable
;   r9     - address - address of the description string
;   rbp+16 - byte    - effect id (value)
;   rbp+17 - dword   - power (value)
;
; Returns:
;   rax - address - address of the new Item structure
;
global NewStructItem
NewStructItem:
    push   rbp
    mov    rbp, rsp

    push   r9
    push   r8
    push   r10
    push   rdx
    push   rsi
    push   rdi

    mov    rdi, ITEM_SIZE
    call   Malloc

    pop    rdi
    mov    byte [rax], dil
    pop    rdi
    mov    qword [rax+OFF_ITEM_NAME], rdi
    pop    rdi
    mov    byte [rax+OFF_QUANTITY], dil
    pop    rdi
    mov    qword [rax+OFF_PRICE], rdi
    pop    rdi
    mov    byte [rax+OFF_IS_SELLABLE], dil
    pop    rdi
    mov    qword [rax+OFF_DESCRIPTION], rdi
    mov    dil, byte [rbp+16]
    mov    byte [rax+OFF_EFFECT_ID], dil
    mov    edi, dword [rbp+17]
    mov    dword [rax+OFF_ITEM_POWER], edi

    pop    rbp
    ret    8


; FreeStructItem
; Item destructor.
;
; Params:
;   rdi - address - address of the Item structure
;
global FreeStructItem
FreeStructItem:
    mov    rsi, ITEM_SIZE
    call   Free

    ret
