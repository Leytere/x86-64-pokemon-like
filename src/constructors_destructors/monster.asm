;------------
section .data

%include "../utils/constants.asm"

extern namesArray

;------------
section .bss

extern Malloc
extern Free
extern GetHpMp
extern GetStats
extern GetXpRequired

;------------
section .text

; NewStructMonster
; Monster constructor.
;
; Params:
;   rdi - value   - id of the monster
;   rsi - value   - level of the monster
;   rdx - address - address of the Stats structure
;
; Returns:
;   rax - address - address of the new monster structure
;
global NewStructMonster
NewStructMonster:
    push   rbx
    push   rdx
    push   rsi
    push   rdi

    mov    rdi, MONSTER_SIZE
    call   Malloc
    mov    rbx, rax

    pop    rdi
    mov    byte [rbx], dil
    mov    rdi, qword [namesArray+(rdi*8)]
    mov    qword [rbx+OFF_MONSTER_NAME], rdi
    pop    rdi
    mov    byte [rbx+OFF_LVL], dil

    pop    rdi
    mov    qword [rbx+OFF_STATS], rdi
    mov    rdi, rbx
    call   GetStats

    mov    rdi, rbx
    call   GetHpMp

    mov    qword [rbx+OFF_CUR_XP], 0
    mov    dil, byte [rbx+OFF_LVL]
    call   GetXpRequired
    mov    qword [rbx+OFF_XP_TO_LVL], rax
    mov    rax, rbx
    
    pop    rbx
    ret

; FreeStructMonster
; Monster destructor.
;
; Params:
;   rdi - address - monster structure address
;
global FreeStructMonster
FreeStructMonster:
    push   rdi
    mov    rdi, qword [rdi+OFF_STATS]
    mov    rsi, STATS_SIZE
    call   Free

    pop    rdi
    mov    rsi, MONSTER_SIZE
    call   Free
    
    ret
